using System.Diagnostics;
using WinFormsStopwatch.Database;
using Timer = System.Threading.Timer;

namespace WinFormsStopwatch
{
    public partial class StopwatchWindow : Form
    {
        private readonly Bitmap _play = new (Properties.Resources.play);
        private readonly Bitmap _pause = new (Properties.Resources.pause);
        private readonly Stopwatch _stopwatch = new();
        private readonly DatabaseService _databaseService = new();
        private TextBox _dialogTextBox;

        public StopwatchWindow()
        {
            InitializeComponent();
            AlignObjects();
            //setting button icon here so I can compare to playpause's image reference correctly on the first click
            PlayPauseButton.Image = _play;
            //hiding save button 
            SavePromptButton.Hide();
        }

        private void PlayPauseClick(object sender, EventArgs e)
        {
            if (PlayPauseButton.Image == _play)
            {
                _stopwatch.Start();
                timer1.Enabled = true;
                PlayPauseButton.Image = _pause;

                SavePromptButton.Hide();
            }
            else
            {
                _stopwatch.Stop();
                timer1.Enabled = false;
                PlayPauseButton.Image = _play;
                UpdateTime(sender, e);

                SavePromptButton.Show();
            }
        }
        private void ResetClick(object sender, EventArgs e)
        {
            _stopwatch.Reset();
            PlayPauseButton.Image = _play;
            UpdateTime(sender, e);
            SavePromptButton.Hide();
        }

        //when timer1.Enabled == true, it calls this method every tick updating time shown
        private void UpdateTime(object sender, EventArgs e)
        {
            TimeTracker.Text = string.Format("{0:hh\\:mm\\:ss\\:ff}", _stopwatch.Elapsed);
        }
        
        private void SaveButtonClick(object sender, EventArgs e)
        {
            var saveDialogForm = new Form
            {
                Size = new Size(ClientSize.Width / 2, ClientSize.Height / 2),
                StartPosition = FormStartPosition.CenterScreen,
                Text = "Save time",
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D,
                MaximizeBox = false,
            };

            var saveButtonOnDialog = new Button
            {
                AutoSize = true,
                Font = new System.Drawing.Font("Bahnschrift", 12F),
                Text = "Save",
                Location = new System.Drawing.Point(140, 150),
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
            };
            saveButtonOnDialog.Click += new System.EventHandler(SaveNameClick);

            var dialogTextBox = new TextBox
            {
                Font = new System.Drawing.Font("Bahnschrift", 12F),
                Name = "NameBox",
                Size = new System.Drawing.Size(saveDialogForm.Width / 2, saveDialogForm.Height / 2),
                Location = new System.Drawing.Point(130, 65),
                PlaceholderText = "e.g. long run"
            };
            _dialogTextBox = dialogTextBox;

            var dialogNameLabel = new Label
            {
                AutoSize = true,
                Font = new System.Drawing.Font("Bahnschrift", 12F),
                Name = "LabelBeforeBox",
                Location = new System.Drawing.Point(40, 67),
                Text = "Name"
            };
            
            saveDialogForm.Controls.Add(saveButtonOnDialog);
            saveDialogForm.Controls.Add(dialogTextBox);
            saveDialogForm.Controls.Add(dialogNameLabel);
            saveDialogForm.Show();
        }

        private void SaveNameClick(object sender, EventArgs e)
        {
            _databaseService.AddTime(new DataFormat{Name = _dialogTextBox.Text, Time = _stopwatch.Elapsed});
            (((sender) as Button).Parent as Form).Close();
            ResetClick(sender, e);
        }

        private void PrevTimesButtonClick(object sender, EventArgs e)
        {
            var prevTimesForm = new Form
            {
                Size = new Size(ClientSize.Width/3, ClientSize.Height),
                StartPosition = FormStartPosition.CenterScreen,
                Text = "Previous times",
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D,
                MaximizeBox = false,
            };

            var deleteAllButton = new Button
            {
                AutoSize = true,
                Font = new System.Drawing.Font("Bahnschrift", 12F),
                Name = "DeleteAllButton",
                Text = "Delete all times",
                Location = new System.Drawing.Point(20, prevTimesForm.Height-105),
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
            };
            deleteAllButton.Click += new System.EventHandler(DeleteAllClick);

            var timesList = new ListBox
            {
                Location = new System.Drawing.Point(0, 0),
                Size = new System.Drawing.Size(prevTimesForm.Width, prevTimesForm.Height-100),
                MultiColumn = true,
                SelectionMode = SelectionMode.MultiExtended
            };

            var listOfTimes = _databaseService.GetTimes();

            timesList.BeginUpdate();
            foreach (var time in listOfTimes)
            {
                if (time.Time > new TimeSpan(1, 0, 0))
                {
                    timesList.Items.Add(string.Format("{0}: {1:hh\\:mm\\:ss\\.ff}", time.Name, time.Time));
                    continue;
                }
                timesList.Items.Add(string.Format("{0}: {1:mm\\:ss\\.ff}", time.Name, time.Time));

            }
            timesList.EndUpdate();

            prevTimesForm.Controls.Add(timesList);
            prevTimesForm.Controls.Add(deleteAllButton);
            prevTimesForm.Show();
        }

        private void DeleteAllClick(object sender, EventArgs e)
        {
            (((sender) as Button).Parent as Form).Close();
            _databaseService.DeleteTimes();
        }

        //aligning everything relative to the window
        private void AlignObjects()
        {
            TimeTracker.Left = (ClientSize.Width - TimeTracker.Width) / 2;
            TimeTracker.Top = (ClientSize.Height - TimeTracker.Height) / 10;

            ResetButton.Left = (ClientSize.Width - ResetButton.Width) / 6;
            ResetButton.Top = 3 * (ClientSize.Height - ResetButton.Height) / 4;

            PlayPauseButton.Left = 5 * (ClientSize.Width - PlayPauseButton.Width) / 6;
            PlayPauseButton.Top = 3 * (ClientSize.Height - PlayPauseButton.Height) / 4;

            SavePromptButton.Left = (ClientSize.Width - SavePromptButton.Width) / 2;
            SavePromptButton.Top = 3 * (ClientSize.Height - SavePromptButton.Height) / 4;
        }
    }
}