﻿namespace WinFormsStopwatch
{
    partial class StopwatchWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PlayPauseButton = new System.Windows.Forms.Button();
            this.TimeTracker = new System.Windows.Forms.Label();
            this.ResetButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SavePromptButton = new System.Windows.Forms.Button();
            this.PrevTimesButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PlayPauseButton
            // 
            this.PlayPauseButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PlayPauseButton.BackColor = System.Drawing.SystemColors.Control;
            this.PlayPauseButton.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.PlayPauseButton.Location = new System.Drawing.Point(606, 309);
            this.PlayPauseButton.Name = "PlayPauseButton";
            this.PlayPauseButton.Size = new System.Drawing.Size(128, 128);
            this.PlayPauseButton.TabIndex = 1;
            this.PlayPauseButton.TabStop = false;
            this.PlayPauseButton.UseVisualStyleBackColor = false;
            this.PlayPauseButton.Click += new System.EventHandler(this.PlayPauseClick);
            // 
            // TimeTracker
            // 
            this.TimeTracker.AutoSize = true;
            this.TimeTracker.Font = new System.Drawing.Font("Bahnschrift", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.TimeTracker.ForeColor = System.Drawing.SystemColors.Desktop;
            this.TimeTracker.Location = new System.Drawing.Point(3, 9);
            this.TimeTracker.Name = "TimeTracker";
            this.TimeTracker.Size = new System.Drawing.Size(768, 173);
            this.TimeTracker.TabIndex = 3;
            this.TimeTracker.Text = "00:00:00.00";
            this.TimeTracker.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ResetButton
            // 
            this.ResetButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ResetButton.BackColor = System.Drawing.SystemColors.Control;
            this.ResetButton.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.ResetButton.Image = global::WinFormsStopwatch.Properties.Resources.stop;
            this.ResetButton.Location = new System.Drawing.Point(46, 309);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(128, 128);
            this.ResetButton.TabIndex = 4;
            this.ResetButton.TabStop = false;
            this.ResetButton.UseVisualStyleBackColor = false;
            this.ResetButton.Click += new System.EventHandler(this.ResetClick);
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.UpdateTime);
            // 
            // SavePromptButton
            // 
            this.SavePromptButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.SavePromptButton.Font = new System.Drawing.Font("Bahnschrift", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.SavePromptButton.Location = new System.Drawing.Point(262, 309);
            this.SavePromptButton.Name = "SavePromptButton";
            this.SavePromptButton.Size = new System.Drawing.Size(256, 128);
            this.SavePromptButton.TabIndex = 5;
            this.SavePromptButton.Text = "Save time";
            this.SavePromptButton.UseVisualStyleBackColor = false;
            this.SavePromptButton.Click += new System.EventHandler(this.SaveButtonClick);
            // 
            // PrevTimesButton
            // 
            this.PrevTimesButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PrevTimesButton.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.PrevTimesButton.Location = new System.Drawing.Point(12, 462);
            this.PrevTimesButton.Name = "PrevTimesButton";
            this.PrevTimesButton.Size = new System.Drawing.Size(257, 66);
            this.PrevTimesButton.TabIndex = 6;
            this.PrevTimesButton.Text = "Show previous times";
            this.PrevTimesButton.UseVisualStyleBackColor = true;
            this.PrevTimesButton.Click += new System.EventHandler(this.PrevTimesButtonClick);
            // 
            // StopwatchWindow
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(774, 540);
            this.Controls.Add(this.PrevTimesButton);
            this.Controls.Add(this.SavePromptButton);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.TimeTracker);
            this.Controls.Add(this.PlayPauseButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "StopwatchWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stopwatch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Button PlayPauseButton;
        private Label TimeTracker;
        private Button ResetButton;
        private System.Windows.Forms.Timer timer1;
        private Button SavePromptButton;
        private Button PrevTimesButton;
    }
}