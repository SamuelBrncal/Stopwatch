﻿using Microsoft.EntityFrameworkCore;

namespace WinFormsStopwatch.Database;

public class StopwatchDbContext : DbContext
{
    public DbSet<DataFormat> SavedTimes { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Stopwatch;Trusted_Connection=True;");
    }
}