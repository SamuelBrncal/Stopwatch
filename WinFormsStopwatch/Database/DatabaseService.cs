﻿using Microsoft.EntityFrameworkCore;

namespace WinFormsStopwatch.Database;

public class DatabaseService
{
    public void AddTime(DataFormat time)
    {
        using var context = new StopwatchDbContext();
        context.SavedTimes.Add(time);
        context.SaveChanges();
    }

    public IList<DataFormat> GetTimes()
    {
        using var context = new StopwatchDbContext();
        return (from t in context.SavedTimes orderby t.ID ascending select t).ToList();
    }

    public void DeleteTimes()
    {
        using var context = new StopwatchDbContext();
        context.Database.ExecuteSqlRaw("DELETE FROM SavedTimes");
        context.Database.ExecuteSqlRaw("DBCC CHECKIDENT('SavedTimes', RESEED, 0)");
    }
}