﻿namespace WinFormsStopwatch.Database;

public class DataFormat
{
    public int ID { get; set; }
    public string Name { get; set; }
    public TimeSpan Time { get; set; }
}